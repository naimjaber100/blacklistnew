﻿using Com.Monty.Omni.Global.Business.Entities.DTOs;

namespace BlackList.Broker.Events
{
    public class MessageEvent
    {
        public Message MessageDto { get; set; }
    }
}

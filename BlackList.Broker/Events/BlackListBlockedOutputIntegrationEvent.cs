﻿using Brokers.EventBus.Events;
using Com.Monty.Omni.Global.Business.Entities.DTOs;

namespace BlackList.Broker.Events
{
    public class BlackListBlockedOutputIntegrationEvent : IntegrationEvent
    {
        public Message MessageDto { get; set; }
    }
}

﻿using Brokers.EventBus.Events;
using Com.Monty.Omni.Global.Business.Entities;

namespace BlackList.Broker.Events
{
    public class BlackListMapIntegrationEvent : IntegrationEvent
    {
      public  BlackListShortUrlMap BlackListShortUrlMap;
    }
}

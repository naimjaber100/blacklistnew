﻿using BlackList.Broker.Contracts.Engines;
using BlackList.Business.Contract.Engines;
using Brokers.EventBus.Abstractions;
using Com.Monty.Omni.Global.Business.Entities.DTOs;
using Common.Business.Common;
using System;
using System.Threading.Tasks;
using Action = Com.Monty.Omni.Global.Common.Enums.Enumerations.Action;
using MessageEvent = Com.Monty.Omni.Global.Broker.Events.MessageEvent;

namespace BlackList.Broker.Handlers
{
    public  class BlackListInputEventHandler : IIntegrationEventHandler<MessageEvent>
    {
        private readonly IBlackListEngine _blackListEngine;
        private readonly ILogAdapter _logAdapter;
        private readonly IBrokerEngine _brokerEngine;

        public BlackListInputEventHandler(IBlackListEngine blackListEngine,  ILogAdapter logAdapter, IBrokerEngine brokerEngine)
        {
            _blackListEngine = blackListEngine;
 
            _logAdapter = logAdapter;
            _brokerEngine = brokerEngine;
        }
            public Task Handle(MessageEvent @event)
            {
                Message message = @event.Message;
            try
            {
                if (message != null)
                {
                    _brokerEngine.PublishReport(message.TenantId, message.CampaignId, message.MessageID, Action.Input);

                    _blackListEngine.Process(message);
                    _brokerEngine.PublishReport(message.TenantId, message.CampaignId, message.MessageID, Action.Processed);
                }
            }
            catch (Exception ex)
            {
                if (message != null)
                {
                    _brokerEngine.PublishReport(message.TenantId, message.CampaignId, message.MessageID, Action.Failed, ex.ToString());
                }
                _logAdapter.WriteLog(ex, "BlackListEventHandler => error");
            }
                return Task.CompletedTask;
            }
        }
    }


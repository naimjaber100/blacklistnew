﻿using BlackList.Broker.Contracts.Engines;
using BlackList.Broker.Events;
using BlackList.Business.Contract.Engines;
using Brokers.EventBus.Abstractions;
using Com.Monty.Omni.Global.Business.Entities;
using Common.Business.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackList.Broker.Handlers
{
    public class BlackListMapInputEventHandler : IIntegrationListEventHandler<BlackListMapIntegrationEvent>
        {
            private readonly IBlackListEngine _blackListEngine;
            private readonly ILogAdapter _logAdapter;
            private readonly IBrokerEngine _brokerEngine;
            public BlackListMapInputEventHandler(IBlackListEngine blackListEngine, ILogAdapter logAdapter, IBrokerEngine brokerEngine)
            {
                _blackListEngine = blackListEngine;
                _logAdapter = logAdapter;
                _brokerEngine = brokerEngine;
            }
            public Task Handle(List<object> @event)
            {
                try
                {
                    if (@event != null)
                    {
                        IEnumerable<BlackListShortUrlMap> messageDto = @event.Cast<BlackListMapIntegrationEvent>().Select(e => e.BlackListShortUrlMap);
                        _blackListEngine.ProcessBlackListMap(messageDto.ToList());
                    }
                }
                catch (Exception ex)
                {
                    _logAdapter.WriteLog(ex, "BlackListEventHandler => error");
                }
                return Task.CompletedTask;
            }
        }
    }


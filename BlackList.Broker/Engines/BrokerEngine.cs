﻿using System;
using IBrokerEngine = BlackList.Broker.Contracts.Engines.IBrokerEngine;
using BlackList.Broker.Events;
using Brokers.EventBus.Configurations;
using System.Threading.Tasks;
using System.Threading;
using BlackList.Broker.Handlers;
using Com.Monty.Omni.Global.Common.Static;
using Com.Monty.Omni.Global.Business.Entities.DTOs;

namespace BlackList.Broker.Engines
{
    public class BrokerEngine : Com.Monty.Omni.Global.Broker.Engines.BrokerEngine,IBrokerEngine
    {
        #region Fields
        #endregion Fields

        #region Constructors

        public BrokerEngine(IServiceProvider serviceProvider)
                             : base(serviceProvider)
        {

        }
        #endregion Constructors

        #region Public Methods

        public void SubscribeNewTenant(string virtualHost)
        {
            EventBusConfiguration eventBusConfiguration = _configurationEngine.GetEventBusConfiguration(virtualHost);

            if (eventBusConfiguration != null)
            {
                RegisterHost(eventBusConfiguration);
                Init();
            }
        }

        public void PublishBlockedInputBroker(Message messageDto)
        {
            ExchangeSetting exchangeSetting = _configurationEngine.GetEventBusExchangeSetting(AppConfigs.OmniChannelSection, AppConfigs.MessageExchangeSettings);
            BlackListBlockedOutputIntegrationEvent blockedInputIntegrationEvent = new()
            {
                MessageDto = messageDto,
            };

            Publish(blockedInputIntegrationEvent, "/", exchangeSetting.QueueName);
        }

        public void PublishSuccessInputBroker(Message messageDto)
        {
            ExchangeSetting exchangeSetting = _configurationEngine.GetEventBusExchangeSetting(AppConfigs.OmniChannelSection, AppConfigs.MessageDNDExchangeSettings);
            BlackListSuccessOutputIntegrationEvent successInputIntegrationEvent = new BlackListSuccessOutputIntegrationEvent()
            {
                MessageDto = messageDto,
            };

            Publish(successInputIntegrationEvent, "/", exchangeSetting.QueueName);
        }

        public void PublishBlackListInputBroker(Message messageDto)
        {
            ExchangeSetting exchangeSetting = _configurationEngine.GetEventBusExchangeSetting(AppConfigs.OmniChannelSection, AppConfigs.BlackListExchangeSettings);
            BlackListInputIntegrationEvent blacklistInputIntegrationEvent = new BlackListInputIntegrationEvent()
            {
                MessageDto = messageDto,
            };
            Publish(blacklistInputIntegrationEvent, "montymobile" , exchangeSetting.QueueName);
        }
        public override Task StartAsync(CancellationToken cancellationToken)
        {
            base.StartAsync(cancellationToken);
            Init();
            return Task.CompletedTask;
        }

        #endregion Public Methods

        #region Private Methods 
        private void Init()
        {
            ExchangeSetting exchangeSettingBlackList = _configurationEngine.GetEventBusExchangeSetting(AppConfigs.OmniChannelSection, AppConfigs.BlackListExchangeSettings);
            Subscribe<Com.Monty.Omni.Global.Broker.Events.MessageEvent, BlackListInputEventHandler>(exchangeSettingBlackList);
            ExchangeSetting exchangeSettingBlackListMap = _configurationEngine.GetEventBusExchangeSetting(AppConfigs.OmniChannelSection, AppConfigs.BlackListShortUrlMapExchangeSettings);
            Subscribe<BlackListMapIntegrationEvent, BlackListMapInputEventHandler>(exchangeSettingBlackListMap);
        }


        #endregion Private Methods
    }
}

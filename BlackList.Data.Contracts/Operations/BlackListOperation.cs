﻿using BlackList.Business.Entities;
using System;

namespace BlackList.Data.Contracts.Operations
{
    public class BlackListOperation : BaseOperation<BlackListSummary>
    {
        public string Source { get; set; }
        public string Destination { get; set; }
        public string Description { get; set; }
        public Com.Monty.Omni.Global.Common.Enums.Enumerations.Channel? Channel { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int tenantid { get; set; }
        public int? AccountId { get; set; }
    }
}

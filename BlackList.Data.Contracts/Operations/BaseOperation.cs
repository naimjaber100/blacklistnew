﻿using Common;
using System.Collections.Generic;

namespace BlackList.Data.Contracts.Operations
{
    public class BaseOperation<T>
    {
        public BaseOperation()
        {
            PageIndex = AppConstants.DEFAULT_PAGE_INDEX;
            PageSize = AppConstants.DEFAULT_PAGE_SIZE;
            OperationSuccess = true;
            Data = new List<T>();
        }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public bool OperationSuccess { get; set; }
        public IEnumerable<T> Data { get; set; }
    }
}

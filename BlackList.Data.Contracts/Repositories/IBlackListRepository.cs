﻿using BlackList.Business.Entities;
using BlackList.Data.Contracts.Operations;
using Com.Monty.Omni.Global.Business.Entities;
using System.Collections.Concurrent;

namespace BlackList.Data.Contracts.Repositories
{
    public  interface IBlackListRepository
    {
        void Get(BlackListOperation blackListOperation);
        int Save(BlackListOperation blackListOperation);
        BlackListSummary Delete(int BlackListId, int accountId);
        ConcurrentDictionary<string, bool> GetSummary(int tenantid, int channelId,int accountId, bool enableCaching = true);
        BlackListShortUrlMap GetBlacklistMap(string generatedCode);
    }
}

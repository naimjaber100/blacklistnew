﻿using System.Collections.Concurrent;

namespace BlackList.Data.Contracts.Repositories
{
    public interface IPrefixRepository
    {      
        ConcurrentDictionary<string, bool> Get(int tenantid,  bool enableCaching = true);
    }
}

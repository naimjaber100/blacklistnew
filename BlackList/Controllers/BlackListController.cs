﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using BlackList.Client.Entities.Requests;
using Common.Business.Services;
using Common.Business.Common;
using Common.Client.Response;
using BlackList.Business.Contract.Services;
using BlackList.Business.Contract.Engines;
using Com.Monty.Omni.Global.Business.Entities.DTOs;
using Microsoft.AspNetCore.Authorization;
using Com.Monty.Omni.Global.Business.Authorization;
using static Com.Monty.Omni.Global.Common.Enums.Enumerations;

namespace BlackList.API.Controllers
{
    [AuthorizeAccountType(AccountType.Business, AccountType.SBusiness)]
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class BlackListController : BaseApiController
    {
        private readonly IBlackListService _blackListService;
        private readonly IValidationEngines _validationEngines;
        public BlackListController(ILogAdapter logAdapter, IJsonAdapter jsonAdapter, IBlackListService blackListService, IValidationEngines validationEngines) : base(logAdapter, jsonAdapter)
        {
            _blackListService = blackListService ?? throw new ArgumentNullException(nameof(blackListService));
            _validationEngines = validationEngines ?? throw new ArgumentNullException(nameof(validationEngines));
        }
  
        [HttpGet]
        public ActionResult<ClientResponse> Get([FromQuery] BlackListRequestInfo blackListRequestInfo)
        {
           return ExecuteOperation(() =>
            {
                string tenantid = IncomingRequestHeader.TenantKey;
                _validationEngines.Validate(IncomingRequestHeader);

                int accountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
                        ?? throw new InvalidOperationException("Invalid AccountID"));

                return _blackListService.Get(Convert.ToInt32(tenantid), accountId,blackListRequestInfo);
            });
        }

        [Route("Validate")]
        [HttpPost]   
        public ActionResult<ClientResponse> Validate(List<BlackListRequest> blackListValidateRequest)
        {
            return ExecuteOperation(() =>
            {
                string tenantid = IncomingRequestHeader.TenantKey;
                _validationEngines.Validate(IncomingRequestHeader);

                int accountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
                         ?? throw new InvalidOperationException("Invalid AccountID"));

                return _blackListService.Validate(Convert.ToInt32(tenantid), accountId, blackListValidateRequest);
            });
        }

        [HttpPost]
        public ActionResult<ClientResponse> SaveList(List<BlackListRequestBulkSave> blackListRequest)
        {
            return ExecuteOperation(() =>
            {
                string tenantid = IncomingRequestHeader.TenantKey;
                _validationEngines.Validate(IncomingRequestHeader);

                int accountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
                        ?? throw new InvalidOperationException("Invalid AccountID"));

                return _blackListService.SaveList(Convert.ToInt32(tenantid), accountId, blackListRequest);
            });
        }

        [HttpPut]
        public ActionResult<ClientResponse> Save(BlackListRequest blackListRequest)
        {
            return ExecuteOperation(() =>
            {
                string tenantid = IncomingRequestHeader.TenantKey;
                _validationEngines.Validate(IncomingRequestHeader);

                int accountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
                      ?? throw new InvalidOperationException("Invalid AccountID"));

                return _blackListService.Save(Convert.ToInt32(tenantid), accountId, blackListRequest);
            });
        }

        [Route("Process")]
        [HttpPost]
        public ActionResult<ClientResponse> Process([FromBody] Message messageDto)
        {
            return ExecuteOperation(() =>
            {
                return _blackListService.Process(messageDto);
            });
        }
  
        [HttpDelete("{Id}")]
        public ActionResult<ClientResponse> Delete([FromRoute] int Id)
        {
            return ExecuteOperation(() =>
            {
                int accountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
                    ?? throw new InvalidOperationException("Invalid AccountID"));

                return _blackListService.Delete(Id, accountId);
            });
        }
    }
}

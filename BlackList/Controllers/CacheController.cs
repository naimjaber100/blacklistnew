﻿using Com.Monty.Omni.Global.Business.Contracts.Services;
using Common.Business.Common;
using Common.Business.Services;
using Common.Client.Response;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackList.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CacheController : BaseApiController
    {
        private readonly ICacheService _cacheService;
        public CacheController(ICacheService CacheService,
                                ILogAdapter LogAdapter,
                                IJsonAdapter JsonAdapter) : base(LogAdapter, JsonAdapter)
        {
            _cacheService = CacheService;
        }


        [HttpGet]
        public ActionResult<ClientResponse> Get(string cacheKey)
        {
            return ExecuteOperation(() =>
            {
                return _cacheService.Get(cacheKey);
            });
        }


        [HttpGet("Keys")]
        public ActionResult<ClientResponse> GetKeys()
        {
            return ExecuteOperation(() =>
            {
                return _cacheService.GetKeys();
            });
        }

        [HttpGet("Keys/{patternKey}")]
        public ActionResult<ClientResponse> GetKeysByPattern([FromRoute] string patternKey)
        {
            return ExecuteOperation(() =>
            {
                return _cacheService.GetKeysByPattern(patternKey);
            });
        }

        [HttpDelete]
        public ActionResult<ClientResponse> Clear(string cacheKey, bool clearAll = false)
        {
            return ExecuteOperation(() =>
            {
                return _cacheService.Clear(cacheKey, clearAll);
            });
        }
    }
}

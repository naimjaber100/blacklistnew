

echo "MT --- STARTING"

#cd MTSmppServer/
docker rmi sms-repo.montymobile.com/omni-blacklist:2.0.0
rm -frd publish/
dotnet publish BlackList.API.csproj -c Release -o publish
docker build -t sms-repo.montymobile.com/omni-blacklist:2.0.0 .
docker push sms-repo.montymobile.com/omni-blacklist:2.0.0
rm -frd publish/
cd ..
cd ..

echo "MT --- FINISHED"

sleep 1d
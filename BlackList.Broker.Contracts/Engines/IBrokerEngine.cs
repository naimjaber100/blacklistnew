﻿using Com.Monty.Omni.Global.Business.Entities.DTOs;

namespace BlackList.Broker.Contracts.Engines
{
    public interface IBrokerEngine: Com.Monty.Omni.Global.Broker.Contracts.Engines.IBrokerEngine
    {
        void PublishBlockedInputBroker(Message messageDto);
        void PublishSuccessInputBroker(Message messageDto);
        void PublishBlackListInputBroker(Message messageDto);
    }
}
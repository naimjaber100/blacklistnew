﻿namespace BlackList.Broker.Contracts.Engines
{
    public  interface IRedisBrokerEngine
    {
        void ReloadRedisSubscribers();
    }
}

﻿namespace BlackList.Common.Configurations
{
    public static class ApplicationConfiguration
    {
        public static string DbConnection { get; set; }
        public static string DbConnectionSchemaName { get; set; }
        public static string ConfigurationDbConnection { get; set; }
        public static string ConfigurationDbConnectionSchemaName { get; set; }
    }
}

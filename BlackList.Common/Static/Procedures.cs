﻿using BlackList.Common.Configurations;

namespace BlackList.Common.Static
{
    public static class Procedures
    {
        public static readonly string GetFilterBlackList = $"{ApplicationConfiguration.DbConnectionSchemaName}.blacklistnumber_get";

        public static readonly string SaveBlackList = $"{ApplicationConfiguration.DbConnectionSchemaName}.blacklistnumber_save";

        public static readonly string GetBlackList = $"{ApplicationConfiguration.DbConnectionSchemaName}.blacklist_get";

        public static readonly string GetPrefix = $"{ApplicationConfiguration.DbConnectionSchemaName}.prefixes_get";

        public static readonly string DeleteBlackList = $"{ApplicationConfiguration.DbConnectionSchemaName}.blacklist_delete";

        public static readonly string GetBlackListMap = $"{ApplicationConfiguration.DbConnectionSchemaName}.blacklist_map_get";
    }
}

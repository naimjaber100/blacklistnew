﻿namespace BlackList.Common.Static
{
    public static class CacheKeys
    {

        public static readonly string PrefixKey = "Prefix";
        public static readonly string BlackListKey = "BlackList" +"-{0}" + "-{1}" + "-{2}";
    }
}

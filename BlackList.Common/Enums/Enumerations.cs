﻿using Monty.Omni.Framework.Common.Enums;

namespace BlackList.Common.Enums
{
    public static class Enumerations
    {
        public enum CollectorDataType
        {
            [CustomDescription(Key = "blacklist")]
            blacklist = 1
        }
        public enum BlackListValidation
        {
            [CustomDescription(Key = "Valid")]
            Valid = 1,
            [CustomDescription(Key = "NotValid")]
            NotValid = 2
        }
    }
}

﻿namespace BlackList.Business.Entities
{
    public class Prefix
    {
        public string PrefixValue { get; set; }
        public string OperatorId { get; set; }
    }
}

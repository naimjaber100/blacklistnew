﻿
using Com.Monty.Omni.Global.Business.Entities;
using System;

namespace BlackList.Business.Entities
{

    [Serializable]
    public class BlackListEntity : BaseEntity
    {
        public long BlacklistId { get; set; }
        public string Source { get; set; }
        public string Destination { get; set; }
        public Com.Monty.Omni.Global.Common.Enums.Enumerations.Channel Channel { get; set; }
        public string Description { get; set; }
        public int AccountId { get; set; }
        public string BlackListIdentifier { get { return string.Format("source{0}destination{1}", this.Source, this.Destination); } }

    }
    public class BlackListSummary: BlackListEntity
    {
        public int TotalRows { get; set; }
    }
}

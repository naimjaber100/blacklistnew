﻿using System.Collections.Generic;

namespace BlackList.Business.Entities
{
    public class LaunchSettings
    {
        public string DbConnection { get; set; }
        public IDictionary<string, string> ConfigurationDbConnection { get; set; }
    }
}

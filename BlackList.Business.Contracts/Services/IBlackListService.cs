﻿using System;
using System.Collections.Generic;
using System.Text;
using BlackList.Client.Entities.Requests;
using Common.Client.Response;
using CloudGateway.Business.Entities;

namespace BlackList.Business.Contracts.Services
{
  public interface IBlackListService
    {
        ClientResponse Get(BlackListRequestInfo blackListRequestInfo);
    }
}

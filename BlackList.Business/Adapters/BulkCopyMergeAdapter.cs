﻿using BlackList.Business.Factories;
using BlackList.Client.Entities.Requests;
using BlackList.Common.Configurations;
using Common.Business.Common;
using Core.Common.Extensions;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CollectorDataType = BlackList.Common.Enums.Enumerations.CollectorDataType;

namespace BlackList.Business.Adapters
{
    public class BulkCopyMergeAdapter<T> : IDisposable
    {
        public IEnumerable<T> InternalStore { get; set; }
        public int BatchSize { get; set; }
        public bool OperationSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public CollectorDataType? DataType { get; set; }
        private readonly ILogAdapter _logAdapter;
        public BulkCopyMergeAdapter(ILogAdapter logAdapter)
        {
            _logAdapter = logAdapter;
        }
        public void Commit()
        {
            try
            {
                if (InternalStore == null || !InternalStore.Any())
                    return;

                using (NpgsqlConnection sqlConnection = new(ApplicationConfiguration.DbConnection))
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                        sqlConnection.Open();

                    using (NpgsqlTransaction transaction = sqlConnection.BeginTransaction())
                    {
                        try
                        {
                            if (BatchSize == 0)
                            {
                                Commit(InternalStore, sqlConnection);
                            }
                            else
                            {
                                var batches = InternalStore.ToList().Batches(BatchSize);
                                foreach (var batch in batches)
                                {
                                    Commit(batch, sqlConnection);
                                }
                            }

                            transaction.Commit();
                        }
                        catch (Exception e)
                        {
                            ErrorMessage = e.Message;
                            OperationSuccess = false;
                            transaction.Rollback();
                            sqlConnection.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = string.Concat(ErrorMessage, " Outer Exception:", ex.Message);
            }
        }
        public void Commit(NpgsqlConnection sqlConnection)
        {
            if (InternalStore == null || !InternalStore.Any())
                return;

            Commit(InternalStore, sqlConnection);
        }
        private void Commit(IEnumerable<T> batch, NpgsqlConnection sqlConnection)
        {
            CollectorDataType collectorDataType = DataType ?? Enum.Parse<CollectorDataType>(typeof(T).Name);
            _logAdapter.WriteDebugLog($"BulkCopyMergeAdapter - {collectorDataType}: Total Internal Size {InternalStore.Count()}, Current Batch Count {batch.Count()}, Batch Size {BatchSize}");

            if (collectorDataType.Equals(CollectorDataType.blacklist))
            {
                var copyHelper = PostgreSqlBulkFactory.GetBlackListRequestPostgreSQLCopyHelper();
                copyHelper.SaveAll(sqlConnection, (IEnumerable<BlackListRequestBulkSave>)batch);
            }
            OperationSuccess = true;
        }
        public void Dispose()
        {
            if (InternalStore != null)
                InternalStore.ToList().Clear();
            InternalStore = null;
        }
    }
}

﻿using BlackList.Business.AutoMapper;
using BlackList.Business.Contract.Engines;
using BlackList.Business.Contract.Services;
using BlackList.Business.Entities;
using BlackList.Client.Entities.Requests;
using BlackList.Client.Entities.Responses;
using BlackList.Data.Contracts.Repositories;
using Com.Monty.Omni.Global.Business.Entities.DTOs;
using Com.Monty.Omni.Global.Business.Services;
using Common.Business.Common;
using Common.Client.Response;
using Monty.Omni.Framework.Business.Contracts.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace BlackList.Business.Services
{
    public class BlackListService : ServiceBase, IBlackListService
    {
        #region Fields 
        private readonly IBlackListEngine _blackListEngine;
        private readonly IBlackListRepository _blackListRepository;
        private readonly IAutoMappers _autoMapper;
        private readonly ILanguageManager _languageManager;

        #endregion Fields 

        #region Constructors
        public BlackListService(ILogAdapter logAdapter, IJsonAdapter jsonAdapter, IBlackListEngine blackListEngine, IBlackListRepository blackListRepository, IAutoMappers autoMapper, ILanguageManager languageManager) : base(logAdapter, jsonAdapter)
        {
            _blackListEngine = blackListEngine ?? throw new ArgumentNullException(nameof(blackListEngine));
            _blackListRepository = blackListRepository ?? throw new ArgumentNullException(nameof(blackListRepository));
            _autoMapper = autoMapper ?? throw new ArgumentNullException(nameof(autoMapper));
            _languageManager = languageManager ?? throw new ArgumentNullException(nameof(languageManager)); ;
        }
        #endregion Constructors

        #region Public Methods

        public ClientResponse Get(int tenantid, int accountId, BlackListRequestInfo blackListRequestInfo)
        {
            List<BlackListSummary> blackList = _blackListEngine.Get(tenantid, accountId,blackListRequestInfo);
            List<BlackListResponseInfo> blackListResponseList = _autoMapper.Mapper.Map<List<BlackListSummary>, List<BlackListResponseInfo>>(blackList);

            PagingResponse pagingResponse = new PagingResponse()
            {
                totalRows = blackList.Count == 0 ? 0 : blackList.FirstOrDefault().TotalRows,
                data = blackListResponseList
            };

            return Success(pagingResponse);
        }

        public ClientResponse SaveList(int tenantid, int accountId, List<BlackListRequestBulkSave> blackListRequest)
        {
            _blackListEngine.SaveList(tenantid, accountId, blackListRequest);

            return Success();
        }

        public ClientResponse Save(int tenantid, int accountId, BlackListRequest blackListRequest)
        {
            BlackListStatusResponseInfo blacklistResponse = _blackListEngine.Save(tenantid, accountId, blackListRequest);
            if (blacklistResponse.ValidData.Count > 0)return Success();
            else return NotFoundOperation(_languageManager.GetPhrase(blacklistResponse.InValidData[0].Reason, Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName));
        }

        public ClientResponse Delete(int BlackListId, int accountId)
        {
            bool success = _blackListEngine.Delete(BlackListId, accountId);
            return success ? Success(BlackListId) : NotFoundOperation(_languageManager.GetPhrase("BlackList Id not found", Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName));
        }

        public ClientResponse Validate(int tenantid,int accountId, List<BlackListRequest> blackListValidateRequest)
        {
            BlackListStatusResponseInfo blacklistResponse = _blackListEngine.Validate(tenantid, accountId, blackListValidateRequest);
            List<BlackListValidateResponseInfo> blackListValidList = _autoMapper.Mapper.Map<List<BlackListRequest>, List<BlackListValidateResponseInfo>>(blacklistResponse.ValidData);
            List<BlackListValidateResponseInfo> blackListInValidList = _autoMapper.Mapper.Map<List<BlackListRequest>, List<BlackListValidateResponseInfo>>(blacklistResponse.InValidData);

            return Success(new
            {
                valid = blackListValidList,
                inValid = blackListInValidList
            });
        }

        public ClientResponse Process(Message messageDto)
        {
            bool success = _blackListEngine.Process(messageDto);

            return success ? Success(_languageManager.GetPhrase("is BlackList", Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) : Success(_languageManager.GetPhrase("is Not BlackList", Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName));
        }
        #endregion Public Methods

    }
}

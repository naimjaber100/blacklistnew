﻿using BlackList.Business.Contract.Engines;
using BlackList.Data.Contracts.Repositories;
using Common.Extensions;
using System.Collections.Concurrent;

namespace BlackList.Business.Engines
{
    public class PrefixEngine: IPrefixEngine
    {
        #region Fields 

        private readonly IPrefixRepository _prefixRepository;
        #endregion Fields 

        #region Constructors
        public PrefixEngine(IPrefixRepository prefixRepository)
        {
            _prefixRepository = prefixRepository;
        }
        #endregion Constructors

        #region Public Methods

        public bool CheckPrefix(string source, string destination, int tenantid)
        {
            bool keySource = false;
            bool keyDestination = false;
            ConcurrentDictionary<string, bool> prefix = _prefixRepository.Get(tenantid, true);
            keySource = IsPrefix(source, prefix);
            keyDestination = IsPrefix(destination, prefix);

            return keySource & keyDestination;
        }
        #endregion Public Methods

        #region Private Methods
        private bool IsPrefix(string number, ConcurrentDictionary<string, bool> ccdictionary)
        {
            bool exist = false;
            try
            {
                // There is a need to check if the number contains at least non-digit character then we shall consider
                // as no match that's why we are using IsDigits function

                if (string.IsNullOrEmpty(number)) return true;
                int numberLength = number.Length;
                string prefix = number;

                if (numberLength >= 8 && numberLength <= 14  &&  number.IsDigits())
                {
                    while (!ccdictionary.ContainsKey(prefix))
                    {
                        numberLength--;
                        if (numberLength == 0)
                        {
                            break;
                        }
                        prefix = number.Substring(0, numberLength);
                    }
                    if (ccdictionary.ContainsKey(prefix))
                    {
                        ccdictionary.TryGetValue(prefix, out exist);
                    }
                }
            }
            catch
            {
                return false;
            }
            return exist;
        }
        #endregion Private Methods
    }
}

﻿using Common.Business.Common;
using Common.Client.Entities;
using Common;
using BlackList.Business.Contract.Engines;

namespace BlackList.Business.Engines
{
    public class ValidationEngine : CommonBase,IValidationEngines
    {
        #region Fields 
        protected readonly ILogAdapter _logAdapter;
        protected readonly IJsonAdapter _jsonAdapter;
        #endregion Fields 
        #region Constructors
        public ValidationEngine(ILogAdapter logAdapter, IJsonAdapter jsonAdapter) : base(logAdapter, jsonAdapter)
        {
            _logAdapter = logAdapter;
            _jsonAdapter = jsonAdapter;
        }

        #endregion Constructors
        #region Public Methods
        public void Validate(RequestHeader incomingRequestHeader)
        {
            if (string.IsNullOrEmpty(incomingRequestHeader.TenantKey))
            {
                BadOperation("Tenant Key should be identified");
            }

            if (string.IsNullOrEmpty(incomingRequestHeader.AuthorizationToken))
            {
                UnAuthorizedOperation("Authorization Token should be identified");
            }
        }
        #endregion Public Methods
    }
}

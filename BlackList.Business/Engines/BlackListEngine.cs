﻿using BlackList.Business.Contract.Engines;
using BlackList.Business.Entities;
using BlackList.Client.Entities.Requests;
using BlackList.Client.Entities.Responses;
using BlackList.Common.Configurations;
using BlackList.Data.Contracts.Operations;
using BlackList.Data.Contracts.Repositories;
using Com.Monty.Omni.Global.Broker.Contracts.Engines;
using Com.Monty.Omni.Global.Business.Engines;
using Com.Monty.Omni.Global.Business.Entities;
using Com.Monty.Omni.Global.Business.Entities.DTOs;
using Dapper;
using Monty.Omni.Framework.Business.Contracts.Managers;
using Npgsql;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Threading;
using static BlackList.Common.Enums.Enumerations;
using static Com.Monty.Omni.Global.Common.Enums.Enumerations;
using Channel = Com.Monty.Omni.Global.Common.Enums.Enumerations.Channel;

namespace BlackList.Business.Engines
{
    public class BlackListEngine : EngineBase, IBlackListEngine
    {

        #region Fields 

        private readonly IBlackListRepository _blackListRepository;
        private readonly IPrefixRepository _prefixRepository;
        private readonly IBrokerEngine _brokerEngine;
        private readonly IPrefixEngine _prefixEngine;
        private readonly ILanguageManager _languageManager;
        #endregion Fields 

        #region Constructors
        public BlackListEngine(IServiceProvider serviceProvider ,
                               IBlackListRepository blackListRepository,
                               IPrefixRepository prefixRepository,
                               IBrokerEngine BrokerEngine,
                               IPrefixEngine prefixEngine,
                               ILanguageManager languageManager
            ) : base(serviceProvider)
        {
            _blackListRepository = blackListRepository ?? throw new ArgumentNullException(nameof(blackListRepository));
            _prefixRepository = prefixRepository ?? throw new ArgumentNullException(nameof(prefixRepository));
            _brokerEngine = BrokerEngine ?? throw new ArgumentNullException(nameof(BrokerEngine));
            _prefixEngine = prefixEngine ?? throw new ArgumentNullException(nameof(prefixEngine));
            _languageManager = languageManager ?? throw new ArgumentNullException(nameof(languageManager));
        }
        #endregion Constructors

        #region Public Methods
        public List<BlackListSummary> Get(int tenantid, int accountId, BlackListRequestInfo blackListRequestInfo)
        {
            BlackListOperation blackListOperation = new BlackListOperation
            {
                PageIndex = blackListRequestInfo.GetPageIndex(),
                PageSize = blackListRequestInfo.GetPageSize(),
                Source = blackListRequestInfo.Source,
                Destination = blackListRequestInfo.Destination,
                Channel = blackListRequestInfo.Channel,
                FromDate = blackListRequestInfo.FromDate,
                ToDate = blackListRequestInfo.ToDate,
                tenantid = tenantid,
                AccountId = accountId
            };

            _blackListRepository.Get(blackListOperation);
            return blackListOperation.Data.ToList();
        }

        public void SaveList(int tenantid, int accountId, List<BlackListRequestBulkSave> blackListRequest)
        {
            blackListRequest.ForEach(c=> c.AccountId = accountId);

            MergeData<BlackListRequestBulkSave>(blackListRequest, accountId, tenantid, "blacklist");
        }

     
        public BlackListStatusResponseInfo Save(int tenantid, int accountId, BlackListRequest blackListRequest)
        {
            BlackListStatusResponseInfo blacklistResponse = new();

            if (blackListRequest.Channel == null)
            {
                blackListRequest.Channel = "All";
            }
            blacklistResponse = Validation(tenantid, blackListRequest);

            if (blacklistResponse.ValidData.Count > 0)
            {
                Channel channel;
                Enum.TryParse(blackListRequest.Channel, true, out channel);
                BlackListOperation blackListOperation = new()
                {
           
                Source = blackListRequest.Source,
                Destination = blackListRequest.Destination,
                Channel = channel,
                Description = blackListRequest.Description,
                tenantid = tenantid,
                AccountId = accountId
            };
                _blackListRepository.Save(blackListOperation);            
            }
            return blacklistResponse;
        }

        public bool Delete( int BlackListId, int accountId)
        {
            BlackListSummary blackListSummary =  _blackListRepository.Delete(BlackListId, accountId);

            return blackListSummary != null;
        }

     
        public bool Process(Message messageDto)
        {
            bool IsBalckList = false;
            try
            {

                IsBalckList = IsBlackList(messageDto);
                if (IsBalckList)
                {
                    messageDto.StatusReason = StatusReason.BlackList;
                    _brokerEngine.PublishMessageValidation(messageDto);
                }
                else
                {
                    _brokerEngine.PublishMessageDNDDelay(messageDto,0);
                }
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
            return IsBalckList;
        }

        public BlackListStatusResponseInfo Validate(int tenantid,int accountId, List<BlackListRequest> blackListRequest)
        {
            BlackListStatusResponseInfo blacklistStatusResponse = new();
            ConcurrentDictionary<string, BlackListRequest> groupedBlackList = new ConcurrentDictionary<string, BlackListRequest>();

            foreach (BlackListRequest blacklistRequest in blackListRequest)
            {
                if (blacklistRequest.Channel == null)
                {
                    blacklistRequest.Channel = "All";
                }
                string Key = string.Format("{0}_{1}_{2}", blacklistRequest.Source, blacklistRequest.Destination, blacklistRequest.Channel);

                if (groupedBlackList.ContainsKey(Key))
                {
                    var reason = "Duplicate";
                    blacklistRequest.Reason = _languageManager.GetPhrase(reason, Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName) ;
                    blacklistStatusResponse.Add((blacklistRequest), BlackListValidation.NotValid);
                    continue;
                }
                else
                {
                    groupedBlackList.TryAdd(Key, blacklistRequest);
                }

                BlackListStatusResponseInfo blacklistResponse = (Validation(tenantid, blacklistRequest));
                if (blacklistResponse.ValidData.Count > 0)
                {
                    blacklistStatusResponse.Add(blacklistResponse.ValidData, BlackListValidation.Valid);
                }
                else
                {
                    blacklistStatusResponse.Add(blacklistResponse.InValidData, BlackListValidation.NotValid);
                }
            }
            return blacklistStatusResponse;
        }

        public void ProcessBlackListMap(List<BlackListShortUrlMap> blackListShortUrlMap)
        {        
            try
            {
                MergeBlackListMapData(blackListShortUrlMap, "blacklist_mapping");
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        #endregion Public Methods

        #region Private Methods
        private bool IsBlackList(Message messageDto)
        {
            bool isBlackList = CheckMatchBlackList
                          (
                             messageDto.OriginalSource,
                             messageDto.OriginalDestination,
                             messageDto.ChannelId,
                             messageDto.TenantId,
                             messageDto.OmniAccountId
                          );

            return isBlackList;
        }
        private bool CheckMatchBlackList(string source, string destination, int channelId, int tenantid, int accountId)
        {
            bool blackListMatch = false;
            string key = string.Format("source{0}destination{1}", source, destination);

            ConcurrentDictionary<string, bool> blacklist = _blackListRepository.GetSummary(tenantid, channelId, accountId, true);
            if (blacklist != null && blacklist.Count > 0)
            {
                blackListMatch = blacklist.ContainsKey(key);
                if (!blackListMatch)
                {
                    key = string.Format("source{0}destination", source);
                    blackListMatch = blacklist.ContainsKey(key);
                }
                if (!blackListMatch)
                {
                    key = string.Format("sourcedestination{0}", destination);
                    blackListMatch = blacklist.ContainsKey(key);
                }
            }

            if (!blackListMatch)
            {
                blacklist = _blackListRepository.GetSummary(tenantid, 0, accountId, true);
                if (blacklist != null && blacklist.Count > 0)
                {
                    key = string.Format("source{0}destination{1}", source, destination);
                    blackListMatch = blacklist.ContainsKey(key);

                    if (!blackListMatch)
                    {
                        key = string.Format("source{0}destination", source);
                        blackListMatch = blacklist.ContainsKey(key);
                    }
                    if (!blackListMatch)
                    {
                        key = string.Format("sourcedestination{0}", destination);
                        blackListMatch = blacklist.ContainsKey(key);
                    }
                }
            }
            return blackListMatch;
        }

        private BlackListStatusResponseInfo Validation(int tenantid, BlackListRequest blacklistRequest)
        {
            blacklistRequest.Channel = blacklistRequest.Channel.Trim();
            bool inValid = false;
            BlackListStatusResponseInfo blacklistResponse = new BlackListStatusResponseInfo();

            if (string.IsNullOrEmpty(blacklistRequest.Source) && string.IsNullOrEmpty(blacklistRequest.Destination))
            {
                var reason = "source and destination should not be empty";
               blacklistRequest.Reason = _languageManager.GetPhrase(reason, Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName) ;
                blacklistResponse.Add(blacklistRequest, BlackListValidation.NotValid);
                inValid = true;
            }
            if (!Enum.GetNames(typeof(Channel)).Any(x => x.ToLower() == blacklistRequest.Channel.ToLower()))
            {
                var reason = "the channel does not exist";
               blacklistRequest.Reason = _languageManager.GetPhrase(reason, Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName) ;
                blacklistResponse.Add(blacklistRequest, BlackListValidation.NotValid);
                inValid = true;
            }

            if (!string.IsNullOrEmpty(blacklistRequest.Source) && blacklistRequest.Source.Equals(blacklistRequest.Destination))
            {
                var reason = "source and destination should not be equal";
                blacklistRequest.Reason = _languageManager.GetPhrase(reason, Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName);
                blacklistResponse.Add(blacklistRequest, BlackListValidation.NotValid);
                inValid = true;
            }

            if (!_prefixEngine.CheckPrefix(blacklistRequest.Source, blacklistRequest.Destination, tenantid))
            {
                var reason = "invalid prefix or (number contains less than 8 digits or greater than 14 digits) for source or destination";
                blacklistRequest.Reason = _languageManager.GetPhrase(reason, Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName);
                blacklistResponse.Add(blacklistRequest, BlackListValidation.NotValid);
                inValid = true;
            }

            if (!inValid)
            {
                var reason = "Valid";
                blacklistRequest.Reason = _languageManager.GetPhrase(reason, Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName);
                blacklistResponse.Add(blacklistRequest, BlackListValidation.Valid);

            }
            return blacklistResponse;
        }


        private void MergeData<T>(IEnumerable<T> objs, int accountId, int tenantid, string destinationTable = "") where T : Com.Monty.Omni.Global.Business.Entities.BaseEntity
        {
            
            try
            {
                string primarykey = "";
                primarykey = "source,destination,channel";

                string fieldsNames = "";
                string updateQuery = "";
                var propertyFeilds = objs.FirstOrDefault().GetType().GetProperties();
                foreach (var item in propertyFeilds)
                {
                    updateQuery += item.Name.ToLower() + " =EXCLUDED." + item.Name.ToLower() + ",";
                    fieldsNames += item.Name.ToLower() + ",";
                }

                string fields = fieldsNames.Trim(',');
                string create_temp = $"CREATE TEMP TABLE temp_{destinationTable} ON COMMIT DROP AS SELECT {fields} FROM {ApplicationConfiguration.DbConnectionSchemaName}.{destinationTable} LIMIT 0";

                string copy = $"COPY temp_{destinationTable} ({fields})  FROM STDIN(FORMAT BINARY)";
                string upsert = $@"INSERT INTO {ApplicationConfiguration.DbConnectionSchemaName}.{destinationTable}  ({fields},createddate)  (SELECT {fields},NOW() FROM temp_{destinationTable}   ) ON CONFLICT({primarykey.ToLower()},tenantid,accountid) DO UPDATE SET 
                                 {updateQuery.Trim(',')}; ";

                using (NpgsqlConnection connection = new(Environment.GetEnvironmentVariable("DbConnection")))
                {
                    connection.Open();
                    using (NpgsqlTransaction transaction = connection.BeginTransaction())
                    {
                        try
                        {
                            connection.Execute(create_temp);
                            using (NpgsqlBinaryImporter writer = connection.BeginBinaryImport(copy))
                            {
                                foreach (var obj in objs)
                                {
                                    obj.TenantId =  Convert.ToInt32( tenantid);
                                    writer.StartRow();
                                    var objFeilds = obj.GetType().GetProperties();
                                    foreach (var objFeild in objFeilds)
                                    {
                                        var isVirtual = typeof(T).GetProperty(objFeild.Name).GetGetMethod().IsVirtual;
                                        if (!isVirtual)
                                        {
                                            string GuidNullableCheck = "";
                                            string objtypeName;
                                            if (GuidNullableCheck == "")
                                            {
                                                objtypeName = objFeild.PropertyType.Name.ToString();
                                            }
                                            else
                                            {
                                                objtypeName = GuidNullableCheck;
                                            }

                                            object objectValue;
                                            DBTypeMappEnums FeildTypeEnum;
                                            if (!Enum.TryParse(objtypeName, out FeildTypeEnum))
                                            {
                                                FeildTypeEnum = DBTypeMappEnums.Int32;
                                                objectValue = (int)objFeild.GetValue(obj);
                                            }
                                            else
                                            {
                                                FeildTypeEnum = (DBTypeMappEnums)Enum.Parse(typeof(DBTypeMappEnums), objtypeName);
                                                objectValue = objFeild.GetValue(obj);
                                            }
                                            NpgsqlTypes.NpgsqlDbType ValueAsEnum = (NpgsqlTypes.NpgsqlDbType)((int)FeildTypeEnum);

                                            writer.Write(objectValue, ValueAsEnum);
                                        }
                                    }
                                }
                                writer.Complete();
                            }
                            connection.Execute(upsert);
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex.InnerException;
                        }
                    }
                    connection.Dispose();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }
        private void MergeBlackListMapData<T>(IEnumerable<T> objs,string destinationTable = "") where T : BaseEntity
        {

            try
            {
                string primarykey = "";
                primarykey = "generated_code";

                string fieldsNames = "";
                string updateQuery = "";
                var propertyFeilds = objs.FirstOrDefault().GetType().GetProperties();
                foreach (var item in propertyFeilds)
                {
                    var isVirtual = typeof(T).GetProperty(item.Name).GetGetMethod().IsVirtual;
                    var name1 = item. GetCustomAttribute<ColumnAttribute>().Name ?? item.Name;
                    if (name1 != objs.FirstOrDefault().GetKeyFieldName().ToLower() && !isVirtual)
                    {
                        var name = item.GetCustomAttribute<ColumnAttribute>().Name ?? item.Name;
                        updateQuery += name + " =EXCLUDED." + name + ",";
                        fieldsNames += name + ",";
                    }
                }

                string fields = fieldsNames.Trim(',');
                string create_temp = $"CREATE TEMP TABLE temp_{destinationTable} ON COMMIT DROP AS SELECT {fields} FROM {ApplicationConfiguration.DbConnectionSchemaName}.{destinationTable} LIMIT 0";

                string copy = $"COPY temp_{destinationTable} ({fields})  FROM STDIN(FORMAT BINARY)";
                string upsert = $@"INSERT INTO {ApplicationConfiguration.DbConnectionSchemaName}.{destinationTable}  ({fields})  (SELECT {fields} FROM temp_{destinationTable}   ) ON CONFLICT({primarykey.ToLower()},tenant_id,account_id) DO UPDATE SET 
                                 {updateQuery.Trim(',')}; ";

                using (NpgsqlConnection connection = new(Environment.GetEnvironmentVariable("DbConnection")))
                {
                    connection.Open();
                    using (NpgsqlTransaction transaction = connection.BeginTransaction())
                    {
                        try
                        {
                            connection.Execute(create_temp);
                            using (NpgsqlBinaryImporter writer = connection.BeginBinaryImport(copy))
                            {
                                foreach (var obj in objs)
                                {
                                    writer.StartRow();
                                    var objFeilds = obj.GetType().GetProperties();
                                    foreach (var objFeild in objFeilds)
                                    {
                                        var isVirtual = typeof(T).GetProperty(objFeild.Name).GetGetMethod().IsVirtual;
                                        if (!isVirtual)
                                        {
                                            string GuidNullableCheck = "";
                                            string objtypeName;
                                            if (GuidNullableCheck == "")
                                            {
                                                objtypeName = objFeild.PropertyType.Name.ToString();
                                            }
                                            else
                                            {
                                                objtypeName = GuidNullableCheck;
                                            }

                                            object objectValue;
                                            DBTypeMappEnums FeildTypeEnum;
                                            if (!Enum.TryParse(objtypeName, out FeildTypeEnum))
                                            {
                                                FeildTypeEnum = DBTypeMappEnums.Int32;
                                                objectValue = (int)objFeild.GetValue(obj);
                                            }
                                            else
                                            {
                                                FeildTypeEnum = (DBTypeMappEnums)Enum.Parse(typeof(DBTypeMappEnums), objtypeName);
                                                objectValue = objFeild.GetValue(obj);
                                            }
                                            NpgsqlTypes.NpgsqlDbType ValueAsEnum = (NpgsqlTypes.NpgsqlDbType)((int)FeildTypeEnum);
                                            writer.Write(objectValue, ValueAsEnum);
                                        }
                                    }
                                }
                                writer.Complete();
                            }
                            connection.Execute(upsert);
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex.InnerException;
                        }
                    }
                    connection.Dispose();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }
        #endregion Private Methods
    }
}

﻿using System;
using PostgreSQLCopyHelper;
using static BlackList.Common.Enums.Enumerations;
using BlackList.Client.Entities.Requests;
using BlackList.Common.Configurations;

namespace BlackList.Business.Factories
{
    public static class PostgreSqlBulkFactory
    {
        private static PostgreSQLCopyHelper<BlackListRequestBulkSave> _blackListPostgreSQLCopyHelper;

        public static PostgreSQLCopyHelper<BlackListRequestBulkSave> GetBlackListRequestPostgreSQLCopyHelper()
        {
            if (_blackListPostgreSQLCopyHelper == null)
            {
              string tableName = CollectorDataType.blacklist.ToString();
                PostgreSQLCopyHelper<BlackListRequestBulkSave> copyHelper = new($"{ApplicationConfiguration.DbConnectionSchemaName}",tableName);


                copyHelper.UsePostgresQuoting();
                copyHelper.MapText("source", x => x.Source)
                          .MapText("destination", x => x.Destination)
                          .MapInteger("channel", x =>(int) x.Channel)
                          .MapText("description", x => x.Description)
                          .MapText("tenantid", x => "10")
                          .MapTimeStamp("createddate", x => DateTime.Now)
                          .MapTimeStamp("updateddate", x => DateTime.Now)
                          ;             
                _blackListPostgreSQLCopyHelper = copyHelper;
            }
            return _blackListPostgreSQLCopyHelper;
        }
    }
}

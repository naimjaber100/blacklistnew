﻿using BlackList.Broker.Contracts.Engines;
using BlackList.Broker.Engines;
using BlackList.Business.AutoMapper;
using BlackList.Business.Contract.Engines;
using BlackList.Business.Contract.Services;
using BlackList.Business.Engines;
using BlackList.Business.Services;
using BlackList.Client.Entities.Responses;
using BlackList.Data.Contracts.Repositories;
using BlackList.Data.Repositories;
using Com.Monty.Omni.Global.Business.Bootstrapper;
using Com.Monty.Omni.Global.Common.Configurations;
using Common.Business.Common;
using Common.Client.Response;
using Common;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using static Com.Monty.Omni.Global.Common.Enums.Enumerations;
using Microsoft.AspNetCore.Hosting;

namespace BlackList.Business.Bootstrapper
{
  public static class StartupExtensions
    {
        public static IServiceCollection Init(this IServiceCollection services, IWebHostEnvironment currentEnvironment)
        {
            ApplicationConfiguration.MicroService = MicroService.BlackList;

            services.InitOmni();
            services.RegisterServices();
            services.ConfigureKeycloakAuthentication(currentEnvironment);
            //services.ConfigureAuthentication();

            return services;
        }

        public static IServiceCollection InitUnitTest(this IServiceCollection services)
        {
            services.InitOmniUnitTest();
            services.RegisterServices();
           
            return services;
        }

        #region Services

        private static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            services.RegisterOmniServices();
            services.AddSingleton<IAutoMappers, AutoMappers>();
            //Engines      
            services.AddSingleton<IBrokerEngine, BrokerEngine>();
            services.AddSingleton<IValidationEngines, ValidationEngine>();
            services.AddSingleton<IPrefixEngine, PrefixEngine>();
            services.AddSingleton<IBlackListEngine, BlackListEngine>();
            //Repositories
            services.AddSingleton<IBlackListRepository, BlackListRepository>();
            services.AddSingleton<IPrefixRepository, PrefixRepository>();
            //Services
            services.AddSingleton<Com.Monty.Omni.Global.Business.Contracts.Services.ICacheService, Com.Monty.Omni.Global.Business.Services.CacheService>();
            services.AddSingleton<Com.Monty.Omni.Global.Business.Contracts.Engines.ICacheEngine, Com.Monty.Omni.Global.Business.Engines.CacheEngine>();
            services.AddSingleton<IBlackListService, BlackListService>();
            services.AddHostedService<BrokerEngine>();

            return services;
        }

        private static IServiceCollection ConfigureAuthentication(this IServiceCollection services)
        {
            var serviceProvider = services.BuildServiceProvider();
            IHttpClientAdapter httpClientAdapter = serviceProvider.GetService<IHttpClientAdapter>();
            IJsonAdapter jsonAdapter = serviceProvider.GetService<IJsonAdapter>();

            string tenantKey = Environment.GetEnvironmentVariable("TenantKey");
            string loginSignature = Environment.GetEnvironmentVariable("LoginSignature");
            string tenantSignature = Environment.GetEnvironmentVariable("TenantSignature");
            string identityBaseUrl = Environment.GetEnvironmentVariable("IdentityBaseUrl");
            string loginTokenUrl = $"{identityBaseUrl}{Environment.GetEnvironmentVariable("LoginTokenUrl")}";
            string getTenantUrl = $"{identityBaseUrl}{Environment.GetEnvironmentVariable("GetTenantUrl")}?tenantKey={tenantKey}";

            #region Calling LoginToken API

            Dictionary<string, string> requestHeaders = new Dictionary<string, string>();
            requestHeaders.Add(RequestHeaders.TenantKey, tenantKey);
            requestHeaders.Add(RequestHeaders.LoginSignature, loginSignature);

            var response = httpClientAdapter.Post(loginTokenUrl, requestHeaders);

            if (!response.StatusCode.Equals(HttpStatusCode.OK))
                throw new Exception("SOMETHING WENT WRONG WHEN CALLING LOGING TOKEN IDENTITY API ON STARTUP");

            ClientResponse clientResponse = jsonAdapter.Deserialize<ClientResponse>(response.Content.ReadAsStringAsync().Result);
            LoginTokenResponseInfo loginTokenResponseInfo = jsonAdapter.Deserialize<LoginTokenResponseInfo>(jsonAdapter.Serialize(clientResponse.Data));

            #endregion Calling LoginToken API

            #region Calling GetTenant API

            requestHeaders.Clear();
            requestHeaders.Add(RequestHeaders.TenantSignature, tenantSignature);
            requestHeaders.Add(RequestHeaders.Authorization, loginTokenResponseInfo.LoginToken);

            response = httpClientAdapter.Get(getTenantUrl, requestHeaders);

            if (!response.StatusCode.Equals(HttpStatusCode.OK))
                throw new Exception("SOMETHING WENT WRONG WHEN CALLING GET TENANT IDENTITY API ON STARTUP");

            clientResponse = jsonAdapter.Deserialize<ClientResponse>(response.Content.ReadAsStringAsync().Result);
            GetTenantResponseInfo getTenantResponseInfo = jsonAdapter.Deserialize<GetTenantResponseInfo>(jsonAdapter.Serialize(clientResponse.Data));

            #endregion Calling GetTenant API

            //ClientResponse

            services
            .AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
           .AddJwtBearer(options =>
           {
               options.RequireHttpsMetadata = false;
               options.SaveToken = true;
               options.TokenValidationParameters = new TokenValidationParameters
               {
                   ValidateIssuer = getTenantResponseInfo.IdentitySettings.ValidateIssuer,
                   ValidateAudience = getTenantResponseInfo.IdentitySettings.ValidateAudience,
                   ValidateIssuerSigningKey = getTenantResponseInfo.IdentitySettings.ValidateSecret,
                   ValidIssuer = getTenantResponseInfo.IdentitySettings.Issuer,
                   ValidAudience = getTenantResponseInfo.IdentitySettings.Audience,
                   IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(getTenantResponseInfo.IdentitySettings.SigningSecret)),
                   ValidateLifetime = true,
                   ClockSkew = TimeSpan.Zero,
               };
           });
            return services;
        }
        #endregion Services
    }
}

dotnet sonarscanner begin /k:"omni_monty.omni.blacklistnew_AYDcTeTx8VHV-oPy97ny" /d:sonar.host.url="https://sonarqube.montymobile.com"  /d:sonar.login="0c337dce73e96a2560ef28fb3012b9ab7a8963f2" /d:sonar.coverageReportPaths=".\sonarqubecoverage\SonarQube.xml"
dotnet build
dotnet test --no-build --collect:"XPlat Code Coverage"
reportgenerator "-reports:*\TestResults\*\coverage.cobertura.xml" "-targetdir:sonarqubecoverage" "-reporttypes:SonarQube"
dotnet sonarscanner end /d:sonar.login="0c337dce73e96a2560ef28fb3012b9ab7a8963f2"
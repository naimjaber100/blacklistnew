﻿namespace BlackList.Client.Entities.Responses
{
    public class LoginTokenResponseInfo
    {
        public string LoginToken { get; set; }
        public int ExpiresIn { get; set; }
    }
}

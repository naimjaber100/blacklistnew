﻿using System.Runtime.Serialization;

namespace BlackList.Client.Entities.Responses
{
    public class BlackListResponseInfo
    {
        [DataMember(Name = "blacklistid")]
        public long BlacklistId { get; set; }
        [DataMember(Name = "source")]
        public string Source { get; set; }
        [DataMember(Name = "destination")]
        public string Destination { get; set; }
        [DataMember(Name = "channel")]
        public string Channel { get; set; }
        [DataMember(Name = "description")]
        public string Description { get; set; }
    }
}

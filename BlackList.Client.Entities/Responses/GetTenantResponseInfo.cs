﻿namespace BlackList.Client.Entities.Responses
{
    public class GetTenantResponseInfo
    {
        public string TenantKey { get; set; }
        public string Name { get; set; }
        public IdentitySettings IdentitySettings { get; set; }
        public string TenantSignature { get; set; }
    }

    public class IdentitySettings
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string SigningSecret { get; set; }
        public bool ValidateIssuer { get; set; }
        public bool ValidateAudience { get; set; }
        public bool ValidateSecret { get; set; }
        public int AccessExpiration { get; set; }
        public string RefreshTokenKey { get; set; }
    }
}

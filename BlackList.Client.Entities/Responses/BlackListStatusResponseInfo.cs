﻿using BlackList.Client.Entities.Requests;
using System.Collections.Generic;
using static BlackList.Common.Enums.Enumerations;

namespace BlackList.Client.Entities.Responses
{
    public class BlackListStatusResponseInfo
    {
        #region Fields 

        public List<BlackListRequest> ValidData;
        public List<BlackListRequest> InValidData;
        #endregion Fields 
    
        #region Constructors
        public BlackListStatusResponseInfo()
        {
            ValidData = new List<BlackListRequest>();
            InValidData = new List<BlackListRequest>();
        }
        #endregion Constructors
        #region Public Methods

        public void Add(
            BlackListRequest blackListItem,
            BlackListValidation blackListType = BlackListValidation.Valid)
        {
            if (blackListType == BlackListValidation.Valid)
                ValidData.Add(blackListItem);
            if (blackListType == BlackListValidation.NotValid)
                InValidData.Add(blackListItem);
        }

        public void Add(
         List<BlackListRequest> blackListItems,
          BlackListValidation blackListType = BlackListValidation.Valid)
        {
            if (blackListItems.Count == 0)
                return;

            if (blackListType == BlackListValidation.Valid)
                ValidData.AddRange(blackListItems);
            if (blackListType == BlackListValidation.NotValid)
                InValidData.AddRange(blackListItems);
        }
        #endregion Public Methods

    }
}

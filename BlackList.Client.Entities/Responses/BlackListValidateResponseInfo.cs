﻿using System.Runtime.Serialization;

namespace BlackList.Client.Entities.Responses
{
    public class BlackListValidateResponseInfo
    {
        [DataMember(Name = "source")]
        public string Source { get; set; }
        [DataMember(Name = "destination")]
        public string Destination { get; set; }
        [DataMember(Name = "channel")]
        public string Channel { get; set; }
        [DataMember(Name = "reason")]
        public string Reason { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }
    }
}

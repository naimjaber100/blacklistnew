﻿using BlackList.Client.Entities.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlackList.Client.Entities.Responses
{
   public class BlackListDuplicateReponseInfo
    {
        #region Fields 

        public List<BlackListRequest> Unchecked;
        public List<BlackListRequest> Duplicate;
        #endregion Fields 

        #region Constructors
        public BlackListDuplicateReponseInfo()
        {
            Unchecked = new List<BlackListRequest>();
            Duplicate = new List<BlackListRequest>();
        }
        #endregion Constructors

        public void Add(
         BlackListRequest blackListItem,int type)
        {
            if (type == 1)
                Unchecked.Add(blackListItem);
            if (type  == 2)
                Duplicate.Add(blackListItem);
        }
    }
}

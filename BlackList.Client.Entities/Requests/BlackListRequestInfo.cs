﻿using System;
using System.Runtime.Serialization;
using Com.Monty.Omni.Global.Client.Entities.Requests;

namespace BlackList.Client.Entities.Requests
{
    [DataContract]
    public class BlackListRequestInfo : BasePagingRequestInfo
    {
        [DataMember(Name = "source")]
        public string Source { get; set; }
        [DataMember(Name = "destination")]
        public string Destination { get; set; }
        [DataMember(Name = "fromDate")]
        public DateTime? FromDate { get; set; }
        [DataMember(Name = "toDate")]
        public DateTime? ToDate { get; set; }
        [DataMember(Name = "channel")]
        public Com.Monty.Omni.Global.Common.Enums.Enumerations.Channel? Channel { get; set; }
        [DataMember(Name = "accountId")]
        public int? AccountId { get; set; }
    }
}

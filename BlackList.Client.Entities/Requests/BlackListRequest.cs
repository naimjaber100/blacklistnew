﻿using System.Runtime.Serialization;

namespace BlackList.Client.Entities.Requests
{
    public class BlackListRequest
    {
        [DataMember(Name = "source")]
        public string Source { get; set; }
        [DataMember(Name = "destination")]
        public string Destination { get; set; }
        [DataMember(Name = "channel")]
        public  string Channel { get; set; }
        [DataMember(Name = "description")]
        public string Description { get; set; }
        [DataMember(Name = "tenantid")]
        public int tenantid { get; set; }
        [DataMember(Name = "reason")]
        public string Reason { get; set; }
    }
}

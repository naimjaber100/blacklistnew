﻿using System.Runtime.Serialization;

namespace BlackList.Client.Entities.Requests
{
    public class BlackListRequestBulkSave : Com.Monty.Omni.Global.Business.Entities.BaseEntity
    {
        [DataMember(Name = "source")]
        public string Source { get; set; }
        [DataMember(Name = "destination")]
        public string Destination { get; set; }
        [DataMember(Name = "description")]
        public string Description { get; set; }
        [DataMember(Name = "channel")]
        public Com.Monty.Omni.Global.Common.Enums.Enumerations.Channel? Channel { get; set; }
        public int AccountId { get; set; }
    }
}

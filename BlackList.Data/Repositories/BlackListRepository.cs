﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using BlackList.Business.Entities;
using BlackList.Common.Static;
using BlackList.Data.Contracts.Operations;
using BlackList.Data.Contracts.Repositories;
using Dapper;
using System.Linq;
using Common.Business.Common;
using Common.Enums;
using Common.Extensions;
using Com.Monty.Omni.Global.Business.Entities;

namespace BlackList.Data.Repositories
{
    public class BlackListRepository : RepositoryBase<BlackListEntity>, IBlackListRepository
    {
        #region Fields
        private readonly ICacheAdapter _cacheAdapter;
        #endregion Fields

        #region Constructors
        public BlackListRepository(Func<CacheType, ICacheAdapter> CacheAdapter)
        {
            _cacheAdapter = CacheAdapter(CacheType.MemoryCache);
        }
        #endregion Constructors

        #region Public Methods

        /// <summary>
        /// Get Black Lists
        /// </summary>
        /// <param name="blackListOperation"></param>
        public void Get(BlackListOperation blackListOperation)
        {
            DynamicParameters dynamicParameters = new DynamicParameters();
            dynamicParameters.Add("_page_index", blackListOperation.PageIndex);
            dynamicParameters.Add("_page_size", blackListOperation.PageSize);
            dynamicParameters.Add("_source", blackListOperation.Source);
            dynamicParameters.Add("_destination", blackListOperation.Destination);
            dynamicParameters.Add("_channel", (int?)blackListOperation.Channel);
            dynamicParameters.Add("_fromdate", blackListOperation.FromDate);
            dynamicParameters.Add("_toddate", blackListOperation.ToDate);
            dynamicParameters.Add("_tenantid", blackListOperation.tenantid);
            dynamicParameters.Add("_accountid", blackListOperation.AccountId);

            blackListOperation.Data = GetList<BlackListSummary>(Procedures.GetFilterBlackList, dynamicParameters);
        }

        /// <summary>
        /// Save Black List
        /// </summary>
        /// <param name="blackListOperation"></param>
        public int Save(BlackListOperation blackListOperation)
        {
            DynamicParameters dynamicParameters = new DynamicParameters();
            dynamicParameters.Add("_source", blackListOperation.Source);
            dynamicParameters.Add("_destination", blackListOperation.Destination);
            dynamicParameters.Add("_channel", (int?)blackListOperation.Channel);
            dynamicParameters.Add("_tenantid", blackListOperation.tenantid);
            dynamicParameters.Add("_description", blackListOperation.Description);
            dynamicParameters.Add("_accountid", blackListOperation.AccountId);

          int result = ExecuteScalar(Procedures.SaveBlackList, dynamicParameters);
            return  result;
        }

        /// <summary>
        /// Delete Black List
        /// </summary>
        /// <param name="BlackListId"></param>
        public BlackListSummary Delete(int BlackListId, int accountId)
        {
            DynamicParameters dynamicParameters = new DynamicParameters();
            dynamicParameters.Add("_blacklistid", BlackListId);
            dynamicParameters.Add("_accountid", accountId);

            var result =  GetSingle<BlackListSummary>(Procedures.DeleteBlackList, dynamicParameters);

            return result;
        }

        public ConcurrentDictionary<string, bool> GetSummary(int tenantid, int channelId,int accountId, bool enableCaching = true)
        {
            if (enableCaching)
            {
                string cacheKey = string.Format(CacheKeys.BlackListKey, tenantid, channelId, accountId);
                return _cacheAdapter.Get(cacheKey, AppConstants.BlacklistEntities_Cache_Time_Minutes,() =>
                {
                    return GetSummary(tenantid, channelId, accountId);
                });
            }
            else
            {
                return GetSummary(tenantid, channelId, accountId);
            }
        }

        public BlackListShortUrlMap GetBlacklistMap(string generatedCode)
        {
            DynamicParameters dynamicParameters = new DynamicParameters();
            dynamicParameters.Add("_code", generatedCode);
            var result = GetSingle<BlackListShortUrlMap>(Procedures.GetBlackListMap, dynamicParameters);

            return result;
        }
        #endregion Public Methods


        #region Private Methods

        private ConcurrentDictionary<string, bool> GetSummary(int tenantid, int channelId,int accountId)
        {
            DynamicParameters dynamicParameters = new DynamicParameters();
            dynamicParameters.Add("_tenantid", tenantid);
            dynamicParameters.Add("_channel", channelId);
            dynamicParameters.Add("_accountid", accountId);
            List<BlackListEntity> blacklists = GetList<BlackListEntity>(Procedures.GetBlackList, dynamicParameters).ToList();

            ConcurrentDictionary<string, bool> groupedBlackList = new ConcurrentDictionary<string, bool>();

            blacklists.ForEach(n =>
            {
                groupedBlackList.TryAdd(n.BlackListIdentifier, true);
            });
            return groupedBlackList;
        }
        #endregion Private Methods
    }
}

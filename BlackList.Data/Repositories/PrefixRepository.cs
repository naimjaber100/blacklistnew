﻿using BlackList.Business.Entities;
using BlackList.Common.Static;
using BlackList.Data.Contracts.Repositories;
using Common.Business.Common;
using Common.Enums;
using Common.Extensions;
using Dapper;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace BlackList.Data.Repositories
{

    public class PrefixRepository : RepositoryBase<Prefix>, IPrefixRepository
    {
        #region Fields 
        private readonly ICacheAdapter _cacheAdapter;
        #endregion Fields 
        #region Constructors
        public PrefixRepository(Func<CacheType, ICacheAdapter> cacheAdapter) 
        {
            _cacheAdapter = cacheAdapter(CacheType.MemoryCache);
        }
        #endregion Constructors

        #region Public Methods

        /// <summary>
        /// Get Prefix
        /// </summary>
        public ConcurrentDictionary<string, bool> Get(int tenantid, bool enableCaching = true)
        {
            if (enableCaching)
            {
                string cacheKey = string.Format("{0}_{1}", CacheKeys.PrefixKey, tenantid);
                return _cacheAdapter.Get(cacheKey, AppConstants.BlacklistEntities_Cache_Time_Minutes, () =>
                {
                    return Get(tenantid);
                });
            }
            else
            {
                return Get(tenantid);
            }
        }
        #endregion Public Methods

        #region Private Methods
        private ConcurrentDictionary<string, bool> Get(int tenantid)
        {
            DynamicParameters dynamicParameters = new DynamicParameters();
            dynamicParameters.Add("p_tenantid", tenantid);

            List<Prefix> prefixes = GetList<Prefix>(Procedures.GetPrefix, dynamicParameters).ToList();

            ConcurrentDictionary<string, bool> numberGroups = new ConcurrentDictionary<string, bool>();
            prefixes.ForEach(n =>
            {
                numberGroups.TryAdd(n.PrefixValue, true);
            });

            return numberGroups;
        }
        #endregion Private Methods
    }
}

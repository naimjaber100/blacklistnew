﻿using Core.Common.Data;

namespace BlackList.Data
{
    public abstract class RepositoryBase<T> : DataRepositoryBase<T, DbConnection>
       where T : class, new()
    {

    }
}

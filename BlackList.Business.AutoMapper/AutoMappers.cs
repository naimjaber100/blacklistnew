﻿using AutoMapper;
using BlackList.Business.Entities;
using BlackList.Client.Entities.Requests;
using BlackList.Client.Entities.Responses;

namespace BlackList.Business.AutoMapper
{
    public class AutoMappers:IAutoMappers
    {
        public IMapper Mapper { get; private set; }
        public AutoMappers()
        {
            ConfigureMappers();
        }

        public void ConfigureMappers()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BlackListSummary, BlackListResponseInfo>();
                cfg.CreateMap<BlackListRequest, BlackListValidateResponseInfo>();
            });

            Mapper = config.CreateMapper();
            config.AssertConfigurationIsValid();
        }
    }
    public interface IAutoMappers
    {
        IMapper Mapper { get; }
    }
}

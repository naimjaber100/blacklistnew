﻿using BlackList.API.Controllers;
using BlackList.Business.Contract.Engines;
using BlackList.Business.Contract.Services;
using Common.Business.Common;
using Moq;
using System;
using Xunit;
using System.Collections.Generic;
using BlackList.XUnitTest.Helpers;
using BlackList.XUnitTest.Extensions;
using BlackList.Client.Entities.Requests;
using Common.Client.Response;
using Microsoft.AspNetCore.Mvc;
using BlackList.Client.Entities.Responses;
using Com.Monty.Omni.Global.Business.Entities.DTOs;

namespace BlackList.XUnitTest.Controller
{
    public class BlackListControllerTest
    {
        #region Fields
        private readonly BlackListController _Controller;
        private readonly Mock<IBlackListService> _mockService;
        private readonly Mock<ILogAdapter> _logAdapter;
        private readonly Mock<IJsonAdapter> _jsonAdapter;
        private readonly Mock<IValidationEngines> _validationEngines;
        private readonly BlackListHelper _BlackListHelper;

        private readonly string _externalId = Environment.GetEnvironmentVariable("externalId") ?? string.Empty;
        private readonly int _tenantId = int.Parse(Environment.GetEnvironmentVariable("tenantId") ?? string.Empty);
        private readonly int _accountId = int.Parse(Environment.GetEnvironmentVariable("accountId") ?? string.Empty);
        private readonly int _blackListId = int.Parse(Environment.GetEnvironmentVariable("blackListId") ?? string.Empty);
        #endregion

        #region Constructors
        public BlackListControllerTest()
        {
            _BlackListHelper = new BlackListHelper();
            _mockService = new Mock<IBlackListService>();
            _logAdapter = new Mock<ILogAdapter>();
            _jsonAdapter = new Mock<IJsonAdapter>();
            _validationEngines = new Mock<IValidationEngines>();

            _Controller = new BlackListController(_logAdapter.Object, _jsonAdapter.Object, _mockService.Object, _validationEngines.Object)
                .WithIdentity(_externalId);
        }
        #endregion

        #region Get BlackList
        [Fact]
        public void GetBlackList_ReturnOK()
        {
            //Arrange
            var mockInfo = new Mock<BlackListRequestInfo>();

            _mockService.Setup(m => m.Get(_tenantId, _accountId, mockInfo.Object))
                .Returns(new ClientResponse { Status = "success", Data = _BlackListHelper.GetBlackList().BlackListres });
            //Act
            var result = _Controller.Get(mockInfo.Object);

            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(result);

            Assert.NotNull(okResult.Value);
            Assert.NotNull(okResult?.Value?.Data);

            List<BlackListResponseInfo> vr = (List<BlackListResponseInfo>)okResult?.Value?.Data ?? throw new ArgumentException("null");

            Assert.Equal(vr.Count, _BlackListHelper.GetBlackList().TotalRows);

            _mockService.Verify(c => c.Get(_tenantId, _accountId, mockInfo.Object), Times.Once);
        }

        [Fact]
        public void GetBlackList_WithoutAccountId_ReturnNull()
        {
            //Arrange
            var mockInfo = new Mock<BlackListRequestInfo>();

            _mockService.Setup(m => m.Get(_tenantId, 0, mockInfo.Object))
                .Returns(new ClientResponse { Status = "success", Data = _BlackListHelper.GetBlackList().BlackListres });
            //Act
            var result = _Controller.Get(mockInfo.Object);

            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(result);

            Assert.Null(okResult.Value);
            Assert.Null(okResult?.Value?.Data);
        }

        [Fact]
        public void GetBlackList_WithoutTenantId_ReturnNull()
        {
            //Arrange
            var mockInfo = new Mock<BlackListRequestInfo>();

            _mockService.Setup(m => m.Get(0, _accountId, mockInfo.Object))
                .Returns(new ClientResponse { Status = "success", Data = _BlackListHelper.GetBlackList().BlackListres });
            //Act
            var result = _Controller.Get(mockInfo.Object);

            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(result);

            Assert.Null(okResult.Value);
            Assert.Null(okResult?.Value?.Data);
        }
        #endregion

        #region Validate BlackList

        [Fact]
        public void ValidateBlackList_ReturnOK()
        {
            //Arrange
            var mockInfo = new Mock<List<BlackListRequest>>();

            _mockService.Setup(m => m.Validate(_tenantId, _accountId, mockInfo.Object))
                .Returns(new ClientResponse { Status = "success", Data = _BlackListHelper.ValidateBlackList() });

            //Act
            var result = _Controller.Validate(mockInfo.Object);

            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(result);

            Assert.NotNull(okResult.Value);
            Assert.NotNull(okResult?.Value?.Data);

            _mockService.Verify(c => c.Validate(_tenantId, _accountId, mockInfo.Object), Times.Once);
        }

        [Fact]
        public void ValidateBlackList_WithoutAccountId_ReturnNull()
        {
            //Arrange
            var mockInfo = new Mock<List<BlackListRequest>>();

            _mockService.Setup(m => m.Validate(_tenantId, 0, mockInfo.Object))
                .Returns(new ClientResponse { Status = "success", Data = _BlackListHelper.ValidateBlackList() });

            //Act
            var result = _Controller.Validate(mockInfo.Object);

            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(result);

            Assert.Null(okResult.Value);
            Assert.Null(okResult?.Value?.Data);
        }

        [Fact]
        public void ValidateBlackList_WithoutTenantId_ReturnNull()
        {
            //Arrange
            var mockInfo = new Mock<List<BlackListRequest>>();

            _mockService.Setup(m => m.Validate(0, _accountId, mockInfo.Object))
                .Returns(new ClientResponse { Status = "success", Data = _BlackListHelper.ValidateBlackList() });

            //Act
            var result = _Controller.Validate(mockInfo.Object);

            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(result);

            Assert.Null(okResult.Value);
            Assert.Null(okResult?.Value?.Data);
        }

        #endregion

        #region SaveList BlackList

        [Fact]
        public void SaveListBlackList_ReturnOK()
        {
            //Arrange
            var mockInfo = new Mock<List<BlackListRequestBulkSave>>();

            _mockService.Setup(m => m.SaveList(_tenantId, _accountId, mockInfo.Object))
                .Returns(new ClientResponse { Status = "success" });

            //Act
            var result = _Controller.SaveList(mockInfo.Object);

            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(result);

            Assert.NotNull(okResult.Value);
            Assert.Equal("success", okResult.Value.Status);

            _mockService.Verify(c => c.SaveList(_tenantId, _accountId, mockInfo.Object), Times.Once);
        }

        [Fact]
        public void SaveListBlackList_WithoutAccountId_ReturnNull()
        {
            //Arrange
            var mockInfo = new Mock<List<BlackListRequestBulkSave>>();

            _mockService.Setup(m => m.SaveList(_tenantId, 0, mockInfo.Object))
                .Returns(new ClientResponse { Status = "success" });

            //Act
            var result = _Controller.SaveList(mockInfo.Object);

            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(result);

            Assert.Null(okResult.Value);
        }

        [Fact]
        public void SaveListBlackList_WithoutTenantId_ReturnNull()
        {
            //Arrange
            var mockInfo = new Mock<List<BlackListRequestBulkSave>>();

            _mockService.Setup(m => m.SaveList(0, _accountId, mockInfo.Object))
                .Returns(new ClientResponse { Status = "success" });

            //Act
            var result = _Controller.SaveList(mockInfo.Object);

            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(result);

            Assert.Null(okResult.Value);
        }

        #endregion

        #region Save BlackList

        [Fact]
        public void SaveBlackList_ReturnOK()
        {
            //Arrange
            var mockInfo = new Mock<BlackListRequest>();

            _mockService.Setup(m => m.Save(_tenantId, _accountId, mockInfo.Object))
                .Returns(new ClientResponse { Status = "success" });

            //Act
            var result = _Controller.Save(mockInfo.Object);

            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(result);

            Assert.NotNull(okResult.Value);
            Assert.Equal("success", okResult.Value.Status);

            _mockService.Verify(c => c.Save(_tenantId, _accountId, mockInfo.Object), Times.Once);
        }

        [Fact]
        public void SaveBlackList_WithoutAccountId_ReturnNull()
        {
            //Arrange
            var mockInfo = new Mock<BlackListRequest>();

            _mockService.Setup(m => m.Save(_tenantId, 0, mockInfo.Object))
                .Returns(new ClientResponse { Status = "success" });

            //Act
            var result = _Controller.Save(mockInfo.Object);

            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(result);

            Assert.Null(okResult.Value);
        }

        [Fact]
        public void SaveBlackList_WithoutTenantId_ReturnNull()
        {
            //Arrange
            var mockInfo = new Mock<BlackListRequest>();

            _mockService.Setup(m => m.Save(0, _accountId, mockInfo.Object))
                .Returns(new ClientResponse { Status = "success" });

            //Act
            var result = _Controller.Save(mockInfo.Object);

            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(result);

            Assert.Null(okResult.Value);
        }

        #endregion

        #region Process BlackList

        [Fact]
        public void ProcessBlackList_ReturnOK()
        {
            //Arrange
            var mockInfo = new Mock<Message>();

            _mockService.Setup(m => m.Process(mockInfo.Object))
                .Returns(new ClientResponse { Status = "success", Data = "is BlackList" });

            //Act
            var result = _Controller.Process(mockInfo.Object);

            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(result);

            Assert.NotNull(okResult.Value);
            Assert.NotNull(okResult?.Value?.Data);
            Assert.Equal("success", okResult.Value.Status);
            Assert.Equal("is BlackList", okResult.Value.Data);

            _mockService.Verify(c => c.Process(mockInfo.Object), Times.Once);
        }
        #endregion

        #region Delete BlackList

        [Fact]
        public void DeleteBlackList_ReturnOK()
        {
            //Arrange
            _mockService.Setup(m => m.Delete(_blackListId, _accountId))
                .Returns(new ClientResponse { Status = "success", Data = "is BlackList" });

            //Act
            var result = _Controller.Delete(1);

            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(result);

            Assert.NotNull(okResult.Value);
            Assert.NotNull(okResult?.Value?.Data);
            Assert.Equal("success", okResult.Value.Status);
            Assert.Equal("is BlackList", okResult.Value.Data);

            _mockService.Verify(c => c.Delete(1, _accountId), Times.Once);
        }

        [Fact]
        public void DeleteBlackList_WithoutBlackListId_ReturnNull()
        {
            //Arrange
            _mockService.Setup(m => m.Delete(0, _accountId))
                .Returns(new ClientResponse { Status = "success", Data = "is BlackList" });

            //Act
            var result = _Controller.Delete(1);

            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(result);

            Assert.Null(okResult.Value);
            Assert.Null(okResult?.Value?.Data);
        }

        [Fact]
        public void DeleteBlackList_WithoutAccountId_ReturnNull()
        {
            //Arrange
            _mockService.Setup(m => m.Delete(_blackListId, 0))
                .Returns(new ClientResponse { Status = "success", Data = "is BlackList" });

            //Act
            var result = _Controller.Delete(1);

            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(result);

            Assert.Null(okResult.Value);
            Assert.Null(okResult?.Value?.Data);
        }

        #endregion
    }
}

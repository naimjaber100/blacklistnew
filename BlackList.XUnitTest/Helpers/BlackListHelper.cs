﻿using BlackList.Business.Entities;
using BlackList.Client.Entities.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackList.Common;
using Common.Client.Response;

namespace BlackList.XUnitTest.Helpers
{
    public class BlackListHelper
    {
        #region Get BlackList Helper

        public class GetBlackListResult
        {
            public int TotalRows { get; set; }
            public List<BlackListResponseInfo> BlackListres { get; set; } = new List<BlackListResponseInfo>();
        }

        public GetBlackListResult _getBlackListResult;

        public GetBlackListResult GetBlackList()
        {
            _getBlackListResult = new GetBlackListResult()
            {
                TotalRows = 3,
                BlackListres = new List<BlackListResponseInfo>()
                {
                    new BlackListResponseInfo()
                    {
                        BlacklistId = 1,
                        Channel = "",
                        Description = "",
                        Destination = "",
                        Source = "",
                    },
                    new BlackListResponseInfo()
                    {
                        BlacklistId = 2,
                        Channel = "",
                        Description = "",
                        Destination = "",
                        Source = "",
                    },
                    new BlackListResponseInfo()
                    {
                        BlacklistId = 3,
                        Channel = "",
                        Description = "",
                        Destination = "",
                        Source = "",
                    }
                }
            };

            return _getBlackListResult;
        }
        #endregion

        #region Validate BlackList Helper

        public class ValidateBlackListResult
        {
            public List<BlackListValidateResponseInfo> valid { get; set; } = new List<BlackListValidateResponseInfo>();
            public List<BlackListValidateResponseInfo> inValid { get; set; } = new List<BlackListValidateResponseInfo>();
        }

        public ValidateBlackListResult _validateBlackListResult;

        public ValidateBlackListResult ValidateBlackList()
        {
            _validateBlackListResult = new ValidateBlackListResult()
            {
                valid = new List<BlackListValidateResponseInfo>()
                {
                    new BlackListValidateResponseInfo
                    {
                        Source = "",
                        Channel = "",
                        Description = "",
                        Destination = "",
                        Reason = "",
                    }
                },
                inValid = new List<BlackListValidateResponseInfo>()
                {
                    new BlackListValidateResponseInfo
                    {
                        Source = "",
                        Channel = "",
                        Description = "",
                        Destination = "",
                        Reason = "",
                    }
                }
            };
            return this._validateBlackListResult;
        }
        #endregion


    }
}

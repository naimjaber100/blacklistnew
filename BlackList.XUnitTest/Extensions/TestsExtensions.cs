﻿using Common.Business.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace BlackList.XUnitTest.Extensions
{
    public static class TestsExtensions
    {
        public static T WithIdentity<T>(this T controller, string externalId) where T : BaseApiController
        {
            controller.EnsureHttpContext();
            
            var principal = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
                            {
                                new Claim("ExternalID", externalId),
                            }, "TestAuthentication"));

            controller.ControllerContext.HttpContext.User = principal;

            return controller;
        }

        public static T WithAnonymousIdentity<T>(this T controller) where T : BaseApiController
        {
            controller.EnsureHttpContext();

            var principal = new ClaimsPrincipal(new ClaimsIdentity());

            controller.ControllerContext.HttpContext.User = principal;

            return controller;
        }

        private static T EnsureHttpContext<T>(this T controller) where T : BaseApiController
        {
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers["tenant-key"] = "1";
            httpContext.Request.Headers["authorization"] = "bearer";
            httpContext.Connection.RemoteIpAddress = System.Net.IPAddress.Parse("127.0.0.1");

            if (controller.ControllerContext == null)
            {
                controller.ControllerContext = new ControllerContext();
            }

            if (controller.ControllerContext.HttpContext == null)
            {
                controller.ControllerContext.HttpContext = httpContext;
            }

            return controller;
        }
    }
}

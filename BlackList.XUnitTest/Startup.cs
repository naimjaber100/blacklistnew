﻿using BlackList.Business.Bootstrapper;
using Com.Monty.Omni.Global.Common.Configurations;
using Com.Monty.Omni.Global.Business.Bootstrapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using Castle.Core.Configuration;
using Microsoft.AspNetCore.Hosting;

namespace BlackList.XUnitTest
{
   public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
        {
            Configuration = configuration;
            CurrentEnvironment = webHostEnvironment;
        }
        public IConfiguration Configuration { get; }
        public IWebHostEnvironment CurrentEnvironment { get; set; }
        public void ConfigureServices(IServiceCollection services)
        {
            Environment.SetEnvironmentVariable("externalId", "1");
            Environment.SetEnvironmentVariable("tenantId", "1");
            Environment.SetEnvironmentVariable("accountId", "1");
            Environment.SetEnvironmentVariable("blackListId", "1");
            Environment.SetEnvironmentVariable("TenantKey", "9ce64459-25d0-4671-a287-8626c4661905");
            Environment.SetEnvironmentVariable("LoginSignature", "fnvaN2S43O7wcQk218XOlhOpMEFV7a/OZc617BNbK4DDfyzFOiqS8AhpsNWAETKMvUIdfm2Dk5oXuKCT4OiDIA==");
            Environment.SetEnvironmentVariable("LoginTokenUrl", "api/v1/Auth/Login");
            Environment.SetEnvironmentVariable("RegisterUrl", "api/v1/Users/Register");
            Environment.SetEnvironmentVariable("IdentityUrl", "api/v1/Users");
            Environment.SetEnvironmentVariable("IdentityBaseUrl", "https://identity-dev.montylocal.net/");
            Environment.SetEnvironmentVariable("TenantSignature", "mhl8djytIggNCFKxjdUizwHKRDjq6V7wT051J1wIuSwjq5swQZH2TJzf/SSkmgTiKzIbSjRcGeMmSSdrnPX0PQ==");
            Environment.SetEnvironmentVariable("GetTenantUrl", "api/v2/Tenants");
            Environment.SetEnvironmentVariable("RolesUrl", "api/v1/Roles");
            Environment.SetEnvironmentVariable("ToggleUrl", "api/v1/Users/ToggleActivation");
            Environment.SetEnvironmentVariable("LoginTokenUrlV2", "api/v2/Auth/Token");
            Environment.SetEnvironmentVariable("DbConnection", "Username=postgres;Password=Z8j7KqgzrTTS9RqY;Server=139.162.181.135;Port=5432;Database=omnidb;");
            Environment.SetEnvironmentVariable("ConfigurationDbConnection", "Username=postgres;Password=Z8j7KqgzrTTS9RqY;Server=139.162.181.135;Port=5432;Database=omni_config_external;");
            Environment.SetEnvironmentVariable("BalanceManagerDbConnection", "Username=postgres;Password=fMJiuFtB4#b@;Server=134.119.218.194;Port=5432;Database=omnidb;");
            
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers["tenant-key"] = "9ce64459-25d0-4671-a287-8626c4661905";
            httpContext.Request.Headers["authorization"] = "bearer";
            httpContext.Connection.RemoteIpAddress = System.Net.IPAddress.Parse("127.0.0.1");


            services.Init(CurrentEnvironment);
            services.InitOmniUnitTest();
        }

    }
}

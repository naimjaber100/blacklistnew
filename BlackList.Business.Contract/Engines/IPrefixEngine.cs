﻿namespace BlackList.Business.Contract.Engines
{
    public interface IPrefixEngine
    {
        bool CheckPrefix(string source, string destination, int tenantid);
    }
}

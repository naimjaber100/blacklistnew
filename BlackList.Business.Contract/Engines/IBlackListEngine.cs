﻿using BlackList.Business.Entities;
using BlackList.Client.Entities.Requests;
using BlackList.Client.Entities.Responses;
using Com.Monty.Omni.Global.Business.Entities;
using Com.Monty.Omni.Global.Business.Entities.DTOs;
using System.Collections.Generic;

namespace BlackList.Business.Contract.Engines
{
    public interface IBlackListEngine
    {
        List<BlackListSummary> Get(int tenantid, int accountId, BlackListRequestInfo blackListRequestInfo);
        void SaveList(int tenantid, int accountId, List<BlackListRequestBulkSave> blackListRequest);
        BlackListStatusResponseInfo Save(int tenantid, int accountId, BlackListRequest blackListRequest);
        bool Delete(int BlackListId, int accountId);
        bool Process(Message messageDto);
        BlackListStatusResponseInfo Validate(int tenantid,int accountId,List<BlackListRequest> blackLisRequest);
        void ProcessBlackListMap(List<BlackListShortUrlMap> blackListShortUrlMap);
    }
}

﻿using Common.Client.Entities;

namespace BlackList.Business.Contract.Engines
{
    public  interface IValidationEngines
    {
        void Validate(RequestHeader incomingRequestHeader);
    }
}

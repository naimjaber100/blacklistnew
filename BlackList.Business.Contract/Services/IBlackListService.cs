﻿using BlackList.Client.Entities.Requests;
using Com.Monty.Omni.Global.Business.Entities.DTOs;
using Common.Client.Response;
using System.Collections.Generic;

namespace BlackList.Business.Contract.Services
{
    public interface IBlackListService
    {
        ClientResponse Get(int tenantid, int accountId, BlackListRequestInfo blackListRequestInfo);
        ClientResponse Validate(int tenantid,int accountId, List<BlackListRequest> blackListValidateRequest);
        ClientResponse SaveList(int tenantid, int accountId, List<BlackListRequestBulkSave> blackListRequest);
        ClientResponse Process(Message messageDto);
        ClientResponse Save(int tenantid, int accountId, BlackListRequest blackListRequest);
        ClientResponse Delete(int BlackListId, int accountId);
    }
}
